﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Forms;
using System.Windows.Threading;

namespace Assignment
{
    public partial class MainWindow : Window
    {
        Dictionary<string, string> storeDict = new Dictionary<string, string>();
        BlockingCollection<string> blockingCollection = new BlockingCollection<string>();
        TaskScheduler scheduler = TaskScheduler.FromCurrentSynchronizationContext();
        HashSet<string> storeNames = new HashSet<string>();
        List<string> suppliers = new List<string>();
        List<string> supplierTypes = new List<string>();
        List<string> weekList = new List<string>();
        List<string> yearList = new List<string>();
        string filePath;
        string folderPath;

        public MainWindow()
        {
            InitializeComponent();
            pieChart.Visibility = Visibility.Hidden;
            btn_back.Visibility = Visibility.Hidden;
        }

        private void chooseFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            var result = openFileDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                filePath = openFileDialog.FileName;
            }
            btn_chooseFolder.IsEnabled = true;
        }

        private void chooseFolderButton_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.RootFolder = Environment.SpecialFolder.Desktop;
            DialogResult result = fbd.ShowDialog();
            while (result != System.Windows.Forms.DialogResult.OK)
            {
                MessageBoxResult message = System.Windows.MessageBox.Show("Try again!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                if (message == MessageBoxResult.OK)
                {
                    result = fbd.ShowDialog();
                    break;
                }
            }

            folderPath = fbd.SelectedPath;
            loadAllDetails();
        }

        private void loadAllDetails()
        {
            progressBar.IsBusy = true;
            string[] fileContents = File.ReadAllLines(filePath);
            btn_chooseFile.IsEnabled = false;
            btn_chooseFolder.IsEnabled = false;
            Parallel.ForEach(fileContents, line =>
            {
                string[] supplierData = line.Split(',');

                if (!storeDict.Keys.Contains(supplierData[0]))
                {
                    storeDict.Add(supplierData[0], supplierData[1]);
                }
            });

            Task.Factory.StartNew(() =>
            {
                Task consumer = Task.Factory.StartNew(() =>
                {
                    consumeFiles();
                });

                Task producer = Task.Factory.StartNew(() =>
                {
                    produceFiles();
                });

                consumer.Wait();
                var li = suppliers.ToList();
                li.Sort();
                li.Insert(0, "All");

                var li2 = supplierTypes.ToList();
                li2.Sort();
                li2.Insert(0, "All");
                var li3 = storeNames.ToList();
                li3.Sort();
                li3.Insert(0, "All");
                var li4 = weekList.Select(int.Parse).ToList();
                li4.Sort();
                var newWeekList = li4.ConvertAll(x => x.ToString());
                newWeekList.Insert(0, "All");
                yearList.Insert(0, "All");

                dispatchIfNecessary(() =>
                {
                    progressBar.IsBusy = false;
                    cb_suppliers.ItemsSource = li;
                    cb_types.ItemsSource = li2;
                    cb_stores.ItemsSource = li3;
                    cb_week.ItemsSource = newWeekList;
                    cb_year.ItemsSource = yearList;
                    btn_calculate.IsEnabled = true;

                    cb_stores.SelectedIndex = 0;
                    cb_suppliers.SelectedIndex = 0;
                    cb_types.SelectedIndex = 0;
                    cb_year.SelectedIndex = 0;
                    cb_week.SelectedIndex = 0;
                    btn_calculate.IsEnabled = true;
                    btn_pieChart.IsEnabled = true;
                    btn_columnChart.IsEnabled = true;
                });
            });
        }

        private void produceFiles()
        {
            foreach (string file in Directory.GetFiles(folderPath))
            {
                blockingCollection.Add(file);
            }

            blockingCollection.CompleteAdding();
        }

        private void consumeFiles()
        {
            while (!blockingCollection.IsCompleted)
            {
                string file;
                if (blockingCollection.TryTake(out file))
                {
                    string[] fileContents2 = File.ReadAllLines(file);
                    string[] fileNameDetails = Path.GetFileName(file).Split('_');
                    string[] year = fileNameDetails[2].Split('.');
                    //adding store name, week and year from the file name
                    if (storeDict.ContainsKey(fileNameDetails[0]))
                    {
                        storeNames.Add(storeDict[fileNameDetails[0]]);
                    }

                    if (!weekList.Contains(fileNameDetails[1]))
                    {
                        weekList.Add(fileNameDetails[1]);
                    }
                    if (!yearList.Contains(year[0]))
                    {
                        yearList.Add(year[0]);
                    }

                    foreach (string line in fileContents2)
                    {
                        //adding suppliers and types from file contents
                        string[] supplierData = line.Split(',');

                        if (!suppliers.Contains(supplierData[0]))
                        {
                            suppliers.Add(supplierData[0]);
                        }
                        if (!supplierTypes.Contains(supplierData[1]))
                        {
                            supplierTypes.Add(supplierData[1]);
                        }
                    }
                }
            }
        }

        private List<string> sortAndInsert(List<string> list)
        {
            list.Sort();
            list.Insert(0, "All");
            return list;
        }

        public void dispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(action);
            }
            else
            {
                action.Invoke();
            }
        }

        private void btn_calculate_Click(object sender, RoutedEventArgs e)
        {
            if ((cb_stores.SelectedIndex != -1) && (cb_stores.SelectedItem.Equals("All")) &&
                (cb_suppliers.SelectedIndex != -1) && (cb_suppliers.SelectedItem.Equals("All")) &&
                (cb_types.SelectedIndex != -1) && (cb_types.SelectedItem.Equals("All")) &&
                (cb_week.SelectedIndex != -1) && (cb_week.SelectedItem.Equals("All")) &&
                 (cb_year.SelectedIndex != -1) && (cb_year.SelectedItem.Equals("All")))
            {
                calculateTotalCostAllOrdersAvailable();
            }


            else if ((cb_stores.SelectedIndex != -1) && (!cb_stores.SelectedItem.Equals("All")) && (cb_types.SelectedIndex != -1) && (!cb_types.SelectedItem.Equals("All")) && (cb_week.SelectedIndex != -1) && (!cb_week.SelectedItem.Equals("All")))
            {
                calculateTotalCostForSupplierTypeForStorePerWeek();
            }

            else if ((cb_stores.SelectedIndex != -1) && (cb_stores.SelectedItem.Equals("All")) && (cb_year.SelectedIndex != -1) && (!cb_year.SelectedItem.Equals("All")))
            {
                calculateTotalAllStoresPerYear();
            }

            else if ((cb_stores.SelectedIndex != -1) && (!cb_stores.SelectedItem.Equals("All")) && (cb_year.SelectedIndex != -1) && (!cb_year.SelectedItem.Equals("All")))
            {
                calculateTotalCostForSingleStorePerYear();
            }
            else if ((cb_stores.SelectedIndex != -1) && (!cb_stores.SelectedItem.Equals("All")) && (cb_week.SelectedIndex != -1) && (!cb_week.SelectedItem.Equals("All")))
            {
                calculateTotalCostSingleStorePerWeek();
            }

            else if ((cb_types.SelectedIndex != -1) && (!cb_types.SelectedItem.Equals("All")) && (cb_week.SelectedIndex != -1) && (!cb_week.SelectedItem.Equals("All")))
            {
                calculateTotalCostOrdersPerWeekForSupplierType();
            }

            else if ((cb_stores.SelectedIndex != -1) && (!cb_stores.SelectedItem.Equals("All")) && (cb_types.SelectedIndex != -1) && (!cb_types.SelectedItem.Equals("All")))
            {
                calculateTotalCostForSupplierTypeForStore();
            }

            else if ((cb_week.SelectedIndex != -1) && (!cb_week.SelectedItem.Equals("All")) && (cb_stores.SelectedItem.Equals("All")))
            {
                calculateTotalCostAllStoresPerWeek();
            }

            else if ((cb_stores.SelectedIndex != -1) && (!cb_stores.SelectedItem.Equals("All")))
            {
                calculateTotalCostAllOrdersSingleStore();
            }

            else if ((cb_suppliers.SelectedIndex != -1) && (!cb_suppliers.SelectedItem.Equals("All")))
            {
                calculateTotalCostOrdersToSupplier();
            }

            else if ((cb_types.SelectedIndex != -1) && (!cb_types.SelectedItem.Equals("All")))
            {
                calculateTotalCostForSupplierType();
            }

            else
            {
                lb_totalCost.Content = "Choose a valid search option!";
            }

            cb_stores.SelectedIndex = 0;
            cb_suppliers.SelectedIndex = 0;
            cb_types.SelectedIndex = 0;
            cb_year.SelectedIndex = 0;
            cb_week.SelectedIndex = 0;
        }

        private void btn_clear_Click(object sender, RoutedEventArgs e)
        {
            cb_stores.SelectedIndex = -1;
            cb_suppliers.SelectedIndex = -1;
            cb_types.SelectedIndex = -1;
            cb_week.SelectedIndex = -1;
            cb_year.SelectedIndex = -1;
        }

        private void calculateTotalCostAllOrdersAvailable()
        {
            double totalCost = 0.00;
            List<Task> tasks = new List<Task>();

            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    tasks.Add(Task<double>.Factory.StartNew(() =>
                    {
                        double totalCostTask = (from line in File.ReadAllLines(file)
                                                let supplierData = line.Split(',')
                                                select double.Parse(supplierData[2])).ToArray().Sum();
                        return totalCostTask;
                    }));
                }
                Task.WaitAll(tasks.ToArray());
            });


            t.ContinueWith((r) =>
            {
                foreach (Task<double> task in tasks)
                {
                    totalCost += task.Result;
                }

                lb_totalCost.Content = "Total cost for all orders available in the supplied data is:\n£" + totalCost.ToString();
            }, scheduler);
        }

        private void calculateTotalCostAllOrdersSingleStore()
        {
            string storeName = cb_stores.SelectedItem.ToString();
            string key = findStoreCode(storeName);
            double totalCost = 0.00;

            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    string[] fileName = Path.GetFileName(file).Split('_');

                    if (fileName[0].Equals(key))
                    {
                        string[] contents = File.ReadAllLines(file);
                        foreach (string line in contents)
                        {
                            string[] supplierData = line.Split(',');
                            totalCost += Convert.ToDouble(supplierData[2]);
                        }
                    }
                }
            });

            t.ContinueWith((r) =>
            {
               lb_totalCost.Content = "Total cost for " + storeName + " store is:\n£ " + totalCost.ToString();
            }, scheduler);
        }

        private void calculateTotalCostAllStoresPerWeek()
        {
            string week = cb_week.SelectedItem.ToString();
            double totalCost = 0.00;

            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    string[] fileName = Path.GetFileName(file).Split('_');

                    if (fileName[1].Equals(week))
                    {
                        string[] contents = File.ReadAllLines(file);
                        foreach (string line in contents)
                        {
                            string[] supplierData = line.Split(',');
                            totalCost += Convert.ToDouble(supplierData[2]);
                        }
                    }
                }
            });

            t.ContinueWith((r) =>
            {
               lb_totalCost.Content = "Total cost for week " + week + " for all stores is:\n£" + totalCost.ToString();
            }, scheduler);
        }

        private void calculateTotalCostSingleStorePerWeek()
        {
            string storeName = cb_stores.SelectedItem.ToString();
            string week = cb_week.SelectedItem.ToString();
            string key = findStoreCode(storeName);
            double totalCost = 0.00;
            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    string[] fileName = Path.GetFileName(file).Split('_');

                    if (fileName[0].Equals(key) && fileName[1].Equals(week))
                    {
                        string[] contents = File.ReadAllLines(file);
                        foreach (string line in contents)
                        {
                            string[] supplierData = line.Split(',');
                            totalCost += Convert.ToDouble(supplierData[2]);
                        }
                    }
                }
            });
            t.ContinueWith((r) =>
            {
                lb_totalCost.Content = "Total cost for " + storeName + " store for week " + week + " is:\n£" + totalCost.ToString();
            }, scheduler);
        }

        private void calculateTotalCostOrdersToSupplier() 
        {
            string supplier = cb_suppliers.SelectedItem.ToString();
            double totalCost = 0.00;
            List<Task> tasks = new List<Task>();

            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    tasks.Add(Task<double>.Factory.StartNew(() =>
                    {
                        double totalCostTask = 0.00;
                        string[] fileContents = File.ReadAllLines(file);
                        foreach (string line in fileContents)
                        {
                            string[] supplierData = line.Split(',');
                            if (supplierData[0].Contains(supplier))
                            {
                                totalCostTask += Convert.ToDouble(supplierData[2]);
                            }
                        }
                        return totalCostTask;
                    }));
                }
                Task.WaitAll(tasks.ToArray());
            });

            t.ContinueWith((r) =>
            {
                foreach (Task<double> task in tasks)
                {
                    totalCost += task.Result;
                }

                lb_totalCost.Content = "Total cost to the supplier " + supplier + " is:\n  £" + totalCost.ToString();
            }, scheduler);

        }

        private void calculateTotalCostForSupplierType()
        {
            string supplierType = cb_types.SelectedItem.ToString();
            double totalCost = 0.00;
            List<Task> tasks = new List<Task>();

            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    tasks.Add(Task<double>.Factory.StartNew(() =>
                    {
                        double totalCostTask = 0.00;
                        string[] fileContents = File.ReadAllLines(file);
                        foreach (string line in fileContents)
                        {
                            string[] supplierData = line.Split(',');
                            if (supplierData[1].Contains(supplierType))
                            {
                                totalCostTask += Convert.ToDouble(supplierData[2]);
                            }
                        }
                        return totalCostTask;
                    }));
                }
                Task.WaitAll(tasks.ToArray());
            });

            t.ContinueWith((r) =>
            {
                foreach (Task<double> task in tasks)
                {
                    totalCost += task.Result;
                }
                lb_totalCost.Content = "Total cost from a supplier type " + supplierType + " is:\n£" + totalCost.ToString();
            }, scheduler);
        }

        private void calculateTotalCostOrdersPerWeekForSupplierType()
        {
            string supplierType = cb_types.SelectedItem.ToString();
            string week = cb_week.SelectedItem.ToString();
            double totalCost = 0.00;

            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    string[] fileName = Path.GetFileName(file).Split('_');

                    if (fileName[1].Equals(week))
                    {
                        string[] contents = File.ReadAllLines(file);
                        foreach (string line in contents)
                        {
                            string[] supplierData = line.Split(',');
                            if (supplierData[1].Contains(supplierType))
                            {
                                totalCost += Convert.ToDouble(supplierData[2]);
                            }
                        }
                    }
                }
            });

            t.ContinueWith((r) =>
            {
                lb_totalCost.Content = "Total cost for supplier type " + supplierType + "\nfor week " + week + " is:\n£" + totalCost.ToString();
            }, scheduler);
        }

        private void calculateTotalCostForSupplierTypeForStore()
        {
            string storeName = cb_stores.SelectedItem.ToString();
            string supplierType = cb_types.SelectedItem.ToString();
            string key = findStoreCode(storeName);
            double totalCost = 0.00;

            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    string[] fileName = Path.GetFileName(file).Split('_');

                    if (fileName[0].Equals(key))
                    {
                        string[] contents = File.ReadAllLines(file);
                        foreach (string line in contents)
                        {
                            string[] supplierData = line.Split(',');
                            if (supplierData[1].Contains(supplierType))
                            {
                                totalCost += Convert.ToDouble(supplierData[2]);
                            }
                        }
                    }
                }
            });
            t.ContinueWith((r) =>
            {
                lb_totalCost.Content = "Total cost for " + storeName + " store for \nsupplier type " + supplierType + " is:\n£" + totalCost.ToString();
            }, scheduler);
        }

        private void calculateTotalCostForSupplierTypeForStorePerWeek()
        {
            string storeName = cb_stores.SelectedItem.ToString();
            string supplierType = cb_types.SelectedItem.ToString();
            string week = cb_week.SelectedItem.ToString();
            string key = findStoreCode(storeName);
            double totalCost = 0.00;

            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    string[] fileName = Path.GetFileName(file).Split('_');

                    if (fileName[0].Equals(key) && fileName[1].Equals(week))
                    {
                        string[] contents = File.ReadAllLines(file);
                        foreach (string line in contents)
                        {
                            string[] supplierData = line.Split(',');
                            if (supplierData[1].Contains(supplierType))
                            {
                                totalCost += Convert.ToDouble(supplierData[2]);
                            }
                        }
                    }
                }
            });

            t.ContinueWith((r) =>
            {
                lb_totalCost.Content = "Total cost for " + storeName + " store for supplier type\n" + supplierType + " for week " + week + " is:\n£" + totalCost.ToString();
            }, scheduler);
        }

        private void calculateTotalCostForSingleStorePerYear()
        {
            string storeName = cb_stores.SelectedItem.ToString();
            string selectedYear = cb_year.SelectedItem.ToString();
            string key = findStoreCode(storeName);
            double totalCost = 0.00;

            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    string[] fileName = Path.GetFileName(file).Split('_');
                    string[] year = fileName[2].Split('.');
                    //adding store name, week and year from the file name

                    if (fileName[0].Equals(key) && year[0].Equals(selectedYear))
                    {
                        string[] contents = File.ReadAllLines(file);
                        foreach (string line in contents)
                        {
                            string[] supplierData = line.Split(',');
                            totalCost += Convert.ToDouble(supplierData[2]);
                        }
                    }
                }
            });
            t.ContinueWith((r) =>
            {
                lb_totalCost.Content = "Total cost for " + storeName + " store for \nyear " + selectedYear + " is:\n£" + totalCost.ToString();
            }, scheduler);
        }

        private void calculateTotalAllStoresPerYear()
        {
            string selectedYear = cb_year.SelectedItem.ToString();
            double totalCost = 0.00;
            List<Task> tasks = new List<Task>();

            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    tasks.Add(Task<double>.Factory.StartNew(() =>
                    {
                        double totalCostTask = 0.00;
                        string[] fileName = Path.GetFileName(file).Split('_');
                        string[] year = fileName[2].Split('.');
                        //adding store name, week and year from the file name

                        if (year[0].Equals(selectedYear))
                        {
                            string[] contents = File.ReadAllLines(file);
                            foreach (string line in contents)
                            {
                                string[] supplierData = line.Split(',');
                                totalCostTask += Convert.ToDouble(supplierData[2]);
                            }
                        }
                        return totalCostTask;
                    }));
                }
                Task.WaitAll(tasks.ToArray());
            });

            t.ContinueWith((r) =>
            {
                foreach (Task<double> task in tasks)
                {
                    totalCost += task.Result;
                }
                lb_totalCost.Content = "Total cost for all stores for year " + selectedYear + " is:\n£" + totalCost.ToString();
            }, scheduler);
        }

        private string findStoreCode(string storeName)
        {
            string key = "";
            dispatchIfNecessary(() =>
            {
                key = storeDict.Where(entry => entry.Value.Equals(storeName))
                    .Select(entry => entry.Key).First();
            });
            return key;
        }

        private void Window_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void btn_pieChart_Click(object sender, RoutedEventArgs e)
        {
            btn_back.Visibility = Visibility.Visible;
            cb_suppliers.Visibility = Visibility.Hidden;
            cb_types.Visibility = Visibility.Hidden;
            cb_stores.Visibility = Visibility.Hidden;
            cb_week.Visibility = Visibility.Hidden;
            cb_year.Visibility = Visibility.Hidden;
            btn_calculate.Visibility = Visibility.Hidden;
            btn_chooseFile.Visibility = Visibility.Hidden;
            btn_chooseFolder.Visibility = Visibility.Hidden;
            btn_clear.Visibility = Visibility.Hidden;
            btn_pieChart.Visibility = Visibility.Hidden;
            lb_totalCost.Visibility = Visibility.Hidden;
            lb_chartLabel.Visibility = Visibility.Visible;
            pieChart.Visibility = Visibility.Visible;
            progressBar.BusyContent = "Loading Pie Chart...";

            lb_chartLabel.Content = "All supplier types total costs for all stores";

            Dictionary<string, double> supplierChart = new Dictionary<string, double>();
            progressBar.IsBusy = true;
            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {                   
                    string[] contents = File.ReadAllLines(file);
                    foreach (string line in contents)
                    {
                        string[] supplierData = line.Split(',');

                        if (!supplierChart.Keys.Contains(supplierData[1]))
                        {
                            supplierChart.Add(supplierData[1], Convert.ToDouble(supplierData[2]));
                        }
                        else
                        {
                            supplierChart[supplierData[1]] += Convert.ToDouble(supplierData[2]);
                        }
                    }                    
                }
            });
            t.ContinueWith((r) =>
            {
                progressBar.IsBusy = false;
                ((PieSeries)pieChart.Series[0]).ItemsSource = supplierChart;

            }, scheduler);
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            cb_suppliers.Visibility = Visibility.Visible;
            cb_types.Visibility = Visibility.Visible;
            cb_stores.Visibility = Visibility.Visible;
            cb_week.Visibility = Visibility.Visible;
            cb_year.Visibility = Visibility.Visible;
            btn_calculate.Visibility = Visibility.Visible;
            btn_chooseFile.Visibility = Visibility.Visible;
            btn_chooseFolder.Visibility = Visibility.Visible;
            btn_clear.Visibility = Visibility.Visible;
            btn_pieChart.Visibility = Visibility.Visible;
            pieChart.Visibility = Visibility.Hidden;
            lb_chartLabel.Visibility = Visibility.Hidden;
            btn_back.Visibility = Visibility.Hidden;
            columnChart.Visibility = Visibility.Hidden;

            lb_totalCost.Visibility = Visibility.Visible;
            lb_totalCost.Content = "";
        }

        private void btn_columnChart_Click(object sender, RoutedEventArgs e)
        {
            btn_back.Visibility = Visibility.Visible;
            cb_suppliers.Visibility = Visibility.Hidden;
            cb_types.Visibility = Visibility.Hidden;
            cb_stores.Visibility = Visibility.Hidden;
            cb_week.Visibility = Visibility.Hidden;
            cb_year.Visibility = Visibility.Hidden;
            btn_calculate.Visibility = Visibility.Hidden;
            btn_chooseFile.Visibility = Visibility.Hidden;
            btn_chooseFolder.Visibility = Visibility.Hidden;
            btn_clear.Visibility = Visibility.Hidden;
            btn_pieChart.Visibility = Visibility.Visible;
            lb_totalCost.Visibility = Visibility.Hidden;
            lb_chartLabel.Visibility = Visibility.Visible;
            pieChart.Visibility = Visibility.Hidden;
            columnChart.Visibility = Visibility.Visible;
            progressBar.BusyContent = "Loading Column Chart...";
            pieChart.Visibility = Visibility.Hidden;
            lb_chartLabel.Content = "Total costs for each supplier type for year 2014";

            Dictionary<string, double> supplierTypePerYearChart = new Dictionary<string, double>();
            progressBar.IsBusy = true;
            Task t = Task.Factory.StartNew(() =>
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    string[] fileName = Path.GetFileName(file).Split('_');
                    string[] year = fileName[2].Split('.');

                    string[] contents = File.ReadAllLines(file);
                    if (year[0].Equals("2014"))
                    {
                        foreach (string line in contents)
                        {
                            string[] supplierData = line.Split(',');
                            if (!supplierTypePerYearChart.Keys.Contains(supplierData[1]))
                            {
                                supplierTypePerYearChart.Add(supplierData[1], Convert.ToDouble(supplierData[2]));
                            }
                            else
                            {
                                supplierTypePerYearChart[supplierData[1]] += Convert.ToDouble(supplierData[2]);
                            }
                        }
                    }
                }
            });

            t.ContinueWith((r) =>
            {
                progressBar.IsBusy = false;
                ((ColumnSeries)columnChart.Series[0]).ItemsSource = supplierTypePerYearChart;

            }, scheduler);
        }
    }
}